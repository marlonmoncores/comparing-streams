package com.br.comparativo.java.streams;

import com.br.comparativo.java.streams.entities.DataToStore;
import com.br.comparativo.java.streams.services.DataToStoreService;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@Rollback(false)
public class StreamsApplicationTests {

	@Autowired
	private DataToStoreService dataToStoreService;


	private long time;
	private String testName;


	@Before
	public void before(){
		if(!dataToStoreService.hasData()) {
			dataToStoreService.createData(100000);
		}
		time=System.currentTimeMillis();
	}

	@After
	public void after(){
		log.info(String.format("TEST %s TOOK %s ms",testName, (System.currentTimeMillis()-time)));
	}

	@Test
	public void findAllOdd(){
		testName = "findAllOdd";
		List<DataToStore> data = dataToStoreService.findAllOdd();
	}

	@Test
	public void findFirst(){
		testName = "findFirst";
		DataToStore data = dataToStoreService.findFirst();
	}

	@Test
	public void findAny(){
		testName = "findAny";
		DataToStore data = dataToStoreService.findAny();
	}

	@Test
	public void findFirstName(){
		testName = "findFirstName";
		String data = dataToStoreService.findFirstName();
	}

}
