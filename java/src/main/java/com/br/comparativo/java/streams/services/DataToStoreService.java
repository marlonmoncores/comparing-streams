package com.br.comparativo.java.streams.services;

import com.br.comparativo.java.streams.entities.DataToStore;
import com.br.comparativo.java.streams.repositories.DataToStoreRepository;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataToStoreService {

    @Autowired
    DataToStoreRepository dataToStoreRepository;

    public void createData(int qtty){
        for(int i=0;i<qtty;i++){
            DataToStore dataToStore = new DataToStore(RandomStringUtils.randomAlphanumeric(30));
            dataToStoreRepository.save(dataToStore);
        }
    }

    public boolean hasData(){
        return dataToStoreRepository.findById(1).isPresent();
    }


    public List<DataToStore> findAllOdd(){
        return dataToStoreRepository.findAll().stream().filter(current -> current.getId()%2 != 0 ).collect(Collectors.toList());
    }

    public DataToStore findFirst(){
        return dataToStoreRepository.findAll().stream().findFirst().get();
    }

    public DataToStore findAny(){
        return dataToStoreRepository.findAll().stream().findFirst().get();
    }

    public String findFirstName(){
        return dataToStoreRepository.findAll().stream().map(DataToStore::getName).findFirst().get();
    }

}
