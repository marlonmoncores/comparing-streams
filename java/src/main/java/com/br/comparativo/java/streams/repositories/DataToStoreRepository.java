package com.br.comparativo.java.streams.repositories;

import com.br.comparativo.java.streams.entities.DataToStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataToStoreRepository extends JpaRepository<DataToStore, Integer> {
}
