package com.br.comparativo.java.streams.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class DataToStore {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;

    public DataToStore (String name){ this.name=name;}

}
