package com.br.comparativo.kotlin.streams.services

import com.br.comparativo.kotlin.streams.entities.DataToStore
import com.br.comparativo.kotlin.streams.repositories.DataToStoreRepository
import org.apache.commons.lang.RandomStringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DataToStoreService(@Autowired
                         var dataToStoreRepository: DataToStoreRepository) {

    fun createData(qtty: Int) {
        for (i in 0 until qtty) {
            val dataToStore = DataToStore(RandomStringUtils.randomAlphanumeric(30))
            dataToStoreRepository.save(dataToStore)
        }
    }

    fun hasData(): Boolean {
        return dataToStoreRepository.findById(1).isPresent()
    }


    fun findAllOdd(): List<DataToStore> {
        return dataToStoreRepository.findAll().filter{ it.id!! % 2 !== 0 }
    }

    fun findFirst(): DataToStore {
        return dataToStoreRepository.findAll().first()
    }

    fun findAny(): DataToStore {
        return dataToStoreRepository.findAll().stream().findFirst().get()
    }

    fun findFirstName(): String {
        return dataToStoreRepository.findAll().map { it.name}.first()
    }

}
