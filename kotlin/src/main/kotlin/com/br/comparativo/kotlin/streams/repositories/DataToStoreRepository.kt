package com.br.comparativo.kotlin.streams.repositories

import com.br.comparativo.kotlin.streams.entities.DataToStore
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DataToStoreRepository : JpaRepository<DataToStore, Int>
