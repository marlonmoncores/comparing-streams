package com.br.comparativo.kotlin.streams

import com.br.comparativo.kotlin.streams.services.DataToStoreService
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@Rollback(false)
class StreamsApplicationTests{
    companion object {
        private val log = LoggerFactory.getLogger(StreamsApplicationTests.javaClass)
    }

    @Autowired
    lateinit var dataToStoreService: DataToStoreService

    private var time: Long = 0
    private var testName: String? = null


    @Before
    fun before() {
        if (!dataToStoreService!!.hasData()) {
            dataToStoreService!!.createData(100000)
        }
        time = System.currentTimeMillis()
    }

    @After
    fun after() {
        log.info(String.format("TEST %s TOOK %s ms", testName, System.currentTimeMillis() - time))
    }

    @Test
    fun findAllOdd() {
        testName = "findAllOdd"
        val data = dataToStoreService.findAllOdd()
    }

    @Test
    fun findFirst() {
        testName = "findFirst"
        val data = dataToStoreService.findFirst()
    }

    @Test
    fun findAny() {
        testName = "findAny"
        val data = dataToStoreService.findAny()
    }

    @Test
    fun findFirstName() {
        testName = "findFirstName"
        val data = dataToStoreService.findFirstName()
    }

}
