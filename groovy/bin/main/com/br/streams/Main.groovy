package com.br.streams

import groovy.transform.*

class Main {                                    
    static void main(String... args) {          
        def a =  new StremTest(createData(100))  
    
        tester(a.&findAny,"findAny ")
       
    }

     static def createData(qtty) {
        def list = [];
      
        while(qtty--){
             list.push(qtty)
        }
        return list;
    }

    static def tester(Closure action,String testName) {
        long time = System.currentTimeMillis();
        action();
        println String.format("TEST %s TOOK %s ms", testName, System.currentTimeMillis()-time )
    }
}


class StremTest {
   def data;
   public StremTest(data) {
       this.data = data;
   }  
   
     def findAllOdd() {
          return data.stream().collect()
       //  return data.findAll().filter{ it.id!! % 2 !== 0 }
    }

     def findFirst() {
          return data.stream().collect()
      //   return data.findAll().first()
    }

     def findAny() {
         return data.stream().collect()
    }

     def findFirstName() {
          return data.stream().collect()
        // return dataToStoreRepository.findAll().map { it.name}.first()
    }
} 
