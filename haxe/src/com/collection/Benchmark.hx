package com.collection;

/**
 * ...
 * @author
 */
using Lambda;
class Benchmark
{

	public function new()
	{
		trace("\n==[Benchmark]==");
		var list = Main.create(1000*10000);
		run(array, list, "array");
		run(count, list, "count");
		run(empty, list, "empty");
		run(has, list, "has");
		run(exists, list, "exists");
		run(indexOf, list, "indexOf");
		run(foreach, list, "foreach");
		run(concat, list, "concat");
		run(filter, list, "filter");
		run(flatMap, list, "flatMap");
		//run(flatten, list, "flatten");
		run(map, list, "map");
		run(mapi, list, "mapi");
		run(fold,list,"fold");
	}

	private function run (action,dataList,label)
	{
		var time = Date.now().getTime();
		action(dataList);
		trace("[ ",label," ] ->",Date.now().getTime() - time );

	}

	private function array (list)
	{
		Lambda.array(list);
	}

	private function count  (list)
	{
		Lambda.count(list);
	}

	private function empty  (list)
	{
		Lambda.empty(list);
	}

	private function has  (list)
	{
		Lambda.count(list);
	}

	private function exists  (list)
	{
		Lambda.exists(list, a -> return a<10);
	}

	private function indexOf  (list)
	{
		Lambda.indexOf(list,50);
	}

	private function find  (list)
	{
		Lambda.find(list, a -> return a>50);
	}

	private function foreach  (list)
	{
		Lambda.foreach(list, a -> return a>50);
	}

	private function concat  (list)
	{
		Lambda.concat(list,list);
	}

	private function filter  (list)
	{
		Lambda.filter(list, a -> return a>50);
	}

	private function flatMap  (list)
	{
		Lambda.flatMap(list, a -> return [a]);
	}

	//private function flatten  (list)
	//{
	//Lambda.flatten([[0],[0]]);
	//}

	private function map (list)
	{
		Lambda.map(list, a -> return a>50);
	}

	private function mapi  (list)
	{
		var mapi = (index,x) -> return x;

		Lambda.mapi(list, mapi);
	}

	private function fold  (list)
	{
		Lambda.fold(list,function(num, total) return total += num,0);
	}

}