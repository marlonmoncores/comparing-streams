package com.collection;

/**
 * ...
 * @author
 */
using Lambda;
class UseCase
{

	public function new()
	{
		trace("\n==[UseCase]==");
		var list = Main.create(1000);
		case1(list); // output -> 20
		case2(list); // output -> 999
		case3(list); // output -> 999
	}

	private function case1  (list)
	{
		var filter = a ->  return a < 5;		
		var result1 = Lambda.filter(list, filter);
		trace("[ case1::filter ]", result1);
		
		var map = a -> return a * 2;			
		var result2 = Lambda.filter(list, filter).map(map);
		trace("[ case1::filter::map ]", result2);
		
		var reduce = (c:Float, p:Float) -> return c+p;		
		var result3 = Lambda.filter(list, filter).map(map).fold(reduce,0);
		trace("[ case1::filter::map::fold ]", result3);
	}

	private function case2  (list)
	{
		var exists = a -> return a > 2;
		var result = Lambda.exists(list, exists);
		trace("[ case2::exists ]", result);
		if (result)
		{
			trace("[ case2::exists::find ]", list.find(exists));
		}
	}

	//@TODO
	private function case3  (list)
	{
		  //trace (Lambda.flatMap([1,2,2], a -> return [a]));
	}

}