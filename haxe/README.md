http://old.haxe.org/doc/cross/lambda
https://github.com/robertbak/haXe-examples/blob/master/src/examples/api/LambdaExamples.hx
http://old.haxe.org/doc/cross/lambda

#Download
https://haxe.org/download/version/4.0.0-preview.4/

#IDE
https://haxe.org/documentation/introduction/editors-and-ides.html

#Install hxjava

    $ haxelib install hxjava

#Building 

    $ haxe  -cp src -java ./comparing-streams/haxe/bin -main com.collection.Main
    $ haxelib run hxjava hxjava_build.txt --haxe-version 4000 --feature-level 1
    $ javac "-sourcepath" "src" "-d" "obj" "-g:none" "@cmd"

    